clc
clear all 
close all
P=41368.54;%Pressure
p=1000;%{kg/m^3}/ Water
g=9.8;%{m^2/s}
R=100;%{m^2/s}
%Variables medidas tanque
Rt=0.13;%{m}
H=0.25;%{m}
L=0.10;%{m}
k1 = p*(2*Rt*L)/(H);
a = 6.32 ;% orificio de salida
c = 0.05 ;%coeficiente del material N/mm^2
g1 = 9810 ;% mm/s^2 gravedad en mm
kout = a*c*sqrt(2*g1) ;