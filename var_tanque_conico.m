clc
clear all 
close all
P=41368.54;%Pressure
p=1000;%{kg/m^3}/ Water
g=9.8;%{m^2/s}
R=1000;%{m^2/s}
Rt=0.13;%{m}
H=0.25;%{m}
L=0.10;%{m}
k1 = p*(2*Rt*L)/(H)
k2 = p*g/R
qo = 10/p 