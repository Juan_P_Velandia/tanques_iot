clc
clear all 
close all
P=41368.54;%Pressure
p=1000;%{kg/m^3}/ Water
g=9.8;%{m^2/s}
R=100;%{m^2/s}
Rt=0.13;%{m}
H=0.25;%{m}
L=0.10;%{m}
k1 = p*(2*Rt*L)/(H);
a = 6.32 ;% orificio de salida
c = 0.05 ;%coeficiente del material N/mm^2
g1 = 9810 ;% mm/s^2 gravedad en mm
kout = a*c*sqrt(2*g) ;

% Variables para la linealizacion
h_bar = 0.28;%Defined by user
qmi = sqrt(h_bar)*kout;

% Definir espacio de estados
A=(-2*qmi + kout*sqrt(h_bar))/(2*h_bar^2*k1);
B=1;
C=1;
D=0;
[num,den] = ss2tf(A,B,C,D);
G = tf(num,den);

%%Define TF of Variator
%%ask to teacher this values
K_V = 10;
T = 0.5;
num_v = [K_V];
den_v = [T 1];
G_V = tf(num_v,den_v);
%%Complete plant
plant = G_V*G;
save('Sys_Variator','G_V')
