%Controlador
% https://www.youtube.com/watch?v=IQYQaZ9g6Cg&t=1084s
%Autor Ingelectronika
clc
close all
clear all
tic
i=0;
Ts=0.01
% Con un tiempo de 0.01
a1=-1.984;
a0=0.9842;
b1=0.001996;
b0=-0.001995;
% inicializacion de se�ales
N = 250 ;
y = zeros (N,1);
u = zeros (N,1);
e = zeros (N,1);
r = zeros (N,1);
v = zeros (N,1);
c = zeros (N,1);
figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
% Referencia
l_r = line(nan,nan,'Color','y','LineWidth',1); 
title('Plant g[]')
xlabel('Time[s]')
subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput u[]')
xlabel('Time[s]')
subplot(2,2,3)
l_e = line(nan,nan,'Color','k','LineWidth',1); 
title('Error')
xlabel('Time[s]')
subplot(2,2,4)
l_v = line(nan,nan,'Color','y','LineWidth',1); 
title('Variator [V]')
xlabel('Time[s]')
t = linspace(0,Ts*N-1,N)'; 
k=1;
%%Parametros para la ley de control
Kp = 1.33e+06; Ki = 1.38e+05; Kd = 2.1e+06; Tf = 0.464; Ts = 0.5;
x1 = Kp*Tf;
x2 = Kp*Ts - Kp*Tf + Ki*Ts*Tf - Kp*Tf;
x3 = -Kp*Ts + Ki*Ts^2 -Ki*Ts*Tf + Kp*Tf;

while 1
    if toc > Ts
        tic
%       Corrimiento de vectores la posicion final queda libre 
        r(1:end-1) = r(2:end);
        r(end) = r(end);
        if k >15
%             r(end)= randi([1 20],1);
            r(end)= 1;
            k=0;
        end
        k = k+1;
        %Definir error
        e(1:end-1) = e(2:end);
        e(end) = r(end)-y(end);

        %la ley de control r2 etc
%         u(end) = (-(Tf-Ts)*u(end-2) -(Ts-2*Tf)*u(end-1) + (x1+Kd)*e(end) + (x2-2*Kd)*e(end-1) +(x3+Kd)*e(end-2))/Tf;
        c(1:end-1) = c(2:end);
        c(end) = 1;
        %Variador
        v(1:end-1) = v(2:end);
        v(end) = (0.9802*v(end-1) + 0.198*c(end));%Ts = 0.01
%         v(end)=1;
        %Planta
        y(1:end-1) = y(2:end);
        y(end) = -a1*y(end-1)-a0*y(end-2)+b1*v(end-1)+b0*v(end-2);%Ts=0.01
        set(l_y,'XData' , t , 'YData' , y)
        set(l_u,'XData' , t , 'YData' , u)
        set(l_e,'XData' , t , 'YData' , e)
        set(l_r,'XData' , t , 'YData' , r)
        set(l_v,'XData' , t , 'YData' , v)
        drawnow
    end  
end