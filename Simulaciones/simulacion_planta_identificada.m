clc
close all
clear all
tic
Ts = 0.01;
i=0;
a1=-1.984;
a0=0.984;
b1=0.001996;
b0=-0.001995;
% inicializacion de se�ales
N = 2000 ;
y = zeros (N,1);
u = zeros (N,1);
figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
title('Plant')
xlabel('Time[s]')
subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput')
xlabel('Time[s]')

t = linspace(0,Ts*N-1,N)'; 
k=0;
while 1
    if toc > Ts
        tic
%       Corrimiento de vectores la posicion final queda libre 
        u(1:end-1) = u(2:end);
        u(end) = u(end);
        if k > 15
            u(end)= 1;
            k=0;
        end
        k = k+1;
        y(1:end-1) = y(2:end);
        y(end) = -a1*y(end-1)-a0*y(end-2)+b1*u(end-1)+b0*u(end-2);
        set(l_y,'XData' , t , 'YData' , y)
        set(l_u,'XData' , t , 'YData' , u)
        drawnow

    end
        
end