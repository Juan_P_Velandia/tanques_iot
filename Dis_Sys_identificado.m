clc
clear all 
close all
% Discretizacion del sistema identificado por tool box
load('Stimate.mat')%Modelo Mat planta estimado
load('Sys_Variator.mat')%Modelo mat propuesto del variador
Ts = 0.01; %sampling interval
Plant_Complet = G_V*tf1;
Gd = c2d(tf1,Ts);
%%Compare the continuos and discrete step responses
step(tf1, 'b',Gd,'r')
Ts_1=0.01
Gdv = c2d(G_V,Ts_1) 
step(G_V,'b',Gdv,'r')
